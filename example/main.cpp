//  Copyright © 2016 hoseking. All rights reserved.

#include <Madmux.h>

#include <chrono>
#include <iostream>
#include <thread>
#include <valarray>

#define DEBUG(msg) std::cout << "main: " << msg << std::endl << std::flush;

bool _process_receive_thread = false;
std::thread _receive_thread;
std::mutex _receive_mutex;
std::condition_variable _receive_cv;

void receive(madmux::Madmux* madmux) {
    std::unique_lock<std::mutex> lock(_receive_mutex);
    do {
        // Read header
		auto header_length = sizeof(uint32_t);
		uint32_t header = 0;
		auto header_bytes_received = madmux->receive((uint8_t*)&header, header_length, 1000);

        // Read data
        if (header_bytes_received > 0) {
            auto data_length = header;
			auto data = std::valarray<uint8_t>(data_length);
			auto data_ptr = &data[0];
			auto data_bytes_received = madmux->receive(data_ptr, data_length, 1000);

            // Output
            if (data_bytes_received > 0) {
                auto data_string = std::string((char*)data_ptr, 0, data_length);
                DEBUG("length: " << data_length << " data " << data_string);
            }
        }

        _receive_cv.wait_for(lock, std::chrono::milliseconds(50));
    } while (_process_receive_thread);
}

int main(int argc, const char * argv[]) {
    madmux::Madmux madmux;
	madmux.setDebug(true);

    auto devices_changed = [&](const int num_devices) {
        DEBUG("devices_changed, number present: " << num_devices);
    };

    auto device_connected = [&]() {
        DEBUG("device_connected");
        _process_receive_thread = true;
        _receive_thread = std::thread(receive, &madmux);
    };

    auto device_disconnected = [&](const int error) {
        DEBUG("device_disconnected, error: " << error);
        {
            std::lock_guard<std::mutex> lock(_receive_mutex);
            _process_receive_thread = false;
        }
        _receive_cv.notify_one();
        _receive_thread.join();
    };

    madmux.connect(2345, devices_changed, device_connected, device_disconnected);

    while (true) {
        std::this_thread::sleep_for(std::chrono::seconds(10));
    }

    return 0;
}
