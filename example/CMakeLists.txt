add_executable(example "main.cpp")
target_compile_definitions(example PRIVATE -Dlibmadmux_DLL)

if(WIN32)
  set(CMAKE_MFC_FLAG 2)
  target_compile_definitions(example PRIVATE -D_AFXDLL -D_UNICODE)
endif()

target_include_directories(example PRIVATE "../madmux")
target_link_libraries(example madmux)

if(WIN32)
  set(LIBS
    "${CMAKE_SOURCE_DIR}/madmux/vendor/lib/libxml2.dll"
    "${CMAKE_SOURCE_DIR}/madmux/vendor/lib/libplist.dll"
    "${CMAKE_SOURCE_DIR}/madmux/vendor/lib/libusbmuxd.dll"
    "${PROJECT_BINARY_DIR}/madmux/${CMAKE_CFG_INTDIR}/madmux.dll")
  add_custom_command(TARGET example POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy ${LIBS} "${PROJECT_BINARY_DIR}/example/${CMAKE_CFG_INTDIR}")
endif()
