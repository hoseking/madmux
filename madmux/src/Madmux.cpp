//  Copyright © 2016 hoseking. All rights reserved.

#include "../Madmux.h"

#include <usbmuxd.h>

#include <algorithm>
#include <cassert>
#include <chrono>
#include <iostream>

namespace madmux {

static bool kDebug = false;
#define DEBUG(msg) if(kDebug) std::cout << "madmux: " << msg << std::endl << std::flush;

void c_callback(const usbmuxd_event_t* usbmuxd_event, void* user_data) {
    if (usbmuxd_event->device.product_id == 0) {
        return;
    }

    auto event = (const Event)usbmuxd_event->event;
    auto device = std::shared_ptr<Device>(new Device{usbmuxd_event->device.handle, std::string(usbmuxd_event->device.udid)});
    auto notification = DeviceNotification{event, device};

    auto cb = *static_cast<std::function<void(const DeviceNotification& notification)>*>(user_data);
    cb(notification);
}

Madmux::Madmux() {
    _connect_thread = std::thread([this]() {
        std::unique_lock<std::mutex> lock(_connect_thread_mutex);
        do {
            bool did_connect = false;
            bool did_disconnect = false;
            int  did_disconnect_error = 0;

            if (_mutex.try_lock()) {

                // Device disconnection
                if (_connection) {
                    auto it = std::find_if(_devices.begin(), _devices.end(), [&](std::shared_ptr<Device> device) {
                        return device->udid == _connection->device->udid;
                    });

                    if (it == _devices.end() || _connection->error != 0) {
                        usbmuxd_disconnect(_connection->fd);
                        did_disconnect = true;
                        did_disconnect_error = _connection->error;
                        _connection.reset();
                    }
                }

                // Device connection
                if (_connection == nullptr) {
                    for (auto& device : _devices) {
                        auto fd = usbmuxd_connect(device->handle, _port);
                        if (fd == -1) {
                            continue;
                        }

                        _connection.reset(new Connection{device, fd, 0});
                        did_connect = true;
                        break;
                    }
                }

                DEBUG("connect thread");
                DEBUG("present:");
                for (auto& device : _devices) {
                    DEBUG("    " << device->udid);
                }
                DEBUG("connected: ");
                if (_connection) {
                    DEBUG("    " << _connection->device->udid);
                }

                _mutex.unlock();
            }

            if (did_disconnect) {
                _device_disconnected(did_disconnect_error);
            }

            if (did_connect) {
                _device_connected();
            }

            _connect_thread_cv.wait_for(lock, std::chrono::seconds(5));
        } while(_process_connect_thread);
    });
}

Madmux::~Madmux() {
    {
        std::lock_guard<std::mutex> lock(_connect_thread_mutex);
        _process_connect_thread = false;
    }
    _connect_thread_cv.notify_one();
    _connect_thread.join();

    _connection.reset();
}

void Madmux::connect(const uint16_t port,
                     DevicesChangedCallback devices_changed,
                     DeviceConnectedCallback device_connected,
                     DeviceDisconnectedCallback device_disconnected) {
    assert(devices_changed     != nullptr);
    assert(device_connected    != nullptr);
    assert(device_disconnected != nullptr);

    _port = port;
    _devices_changed = devices_changed;
    _device_connected = device_connected;
    _device_disconnected = device_disconnected;

    auto device_added = [this](std::shared_ptr<Device> device) {
        addDevice(device);
    };

    auto device_removed = [this](std::shared_ptr<Device> device) {
        removeDevice(device);
    };

    _device_notification = [=](const DeviceNotification& notification) {
        switch (notification.event) {
            case Event::kDeviceAdded:
                device_added(notification.device);
                break;
            case Event::kDeviceRemoved:
                device_removed(notification.device);
                break;
        }
    };
    usbmuxd_subscribe(c_callback, &_device_notification);
}

bool Madmux::connected() const {
    return _connection && _connection->error != 0;
}

uint32_t Madmux::send(const uint8_t* data, const uint32_t length) {
    std::shared_ptr<Connection> connection = nullptr;
    {
        std::lock_guard<std::mutex> lock(_mutex);
        if (_connection == nullptr) {
            return 0;
        }
        connection = _connection;
    }

    uint32_t bytes_sent = 0;
    auto result = usbmuxd_send(connection->fd, (char*)data, length, &bytes_sent);
    if (result < 0) {
        connection->error = result;
    }

    DEBUG("sent " << bytes_sent << " bytes");
    return bytes_sent;
}

uint32_t Madmux::receive(const uint8_t* data, const uint32_t length, const uint32_t timeout) {
    std::shared_ptr<Connection> connection = nullptr;
    {
        std::lock_guard<std::mutex> lock(_mutex);
        if (_connection == nullptr) {
            return 0;
        }
        connection = _connection;
    }

    uint32_t bytes_received = 0;
    auto result = usbmuxd_recv_timeout(connection->fd, (char*)data, length, &bytes_received, timeout);
    if (result < 0) {
        connection->error = result;
    }

    DEBUG("received " << bytes_received << " bytes");
    return bytes_received;
}

void Madmux::setDebug(bool enabled) {
	kDebug = enabled;
}

void Madmux::addDevice(const std::shared_ptr<Device> device) {
    auto device_count = 0;
    {
        std::lock_guard<std::mutex> lock(_mutex);

        auto it = std::find_if(_devices.begin(), _devices.end(), [&](std::shared_ptr<Device> d) {
            return d->udid == device->udid;
        });

        if (it == _devices.end()) {
            DEBUG("added device, uuid: " << device->udid);
            _devices.push_back(device);
        }

        device_count = _devices.size();
    }
    _devices_changed(device_count);
    _connect_thread_cv.notify_one();
}

void Madmux::removeDevice(const std::shared_ptr<Device> device) {
    auto device_count = 0;
    {
        std::lock_guard<std::mutex> lock(_mutex);

        auto it = std::find_if(_devices.begin(), _devices.end(), [&](std::shared_ptr<Device> d) {
            return d->udid == device->udid;
        });

        if (it != _devices.end()) {
            DEBUG("removed device, uuid: " << device->udid);
            _devices.erase(it);
        }

        device_count = _devices.size();
    }
    _devices_changed(device_count);
    _connect_thread_cv.notify_one();
}

}
