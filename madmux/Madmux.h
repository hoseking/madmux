//  Copyright © 2016 hoseking. All rights reserved.

#pragma once

#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#ifdef WIN32
	#define madmux_DLL_EXPORT __declspec(dllexport)
	#define madmux_DLL_IMPORT __declspec(dllimport)
#else
	#define madmux_DLL_EXPORT __attribute__ ((visibility ("default")))
	#define madmux_DLL_IMPORT __attribute__ ((visibility ("default")))
#endif

#ifdef madmux_DLL // defined if compiled as a dynamic lib
	#ifdef madmux_DLL_EXPORTS // defined if building a dynamic lib
		#define madmux_API madmux_DLL_EXPORT
	#else
		#define madmux_API madmux_DLL_IMPORT
	#endif
#else
	#define madmux_API
#endif

namespace madmux {

enum class Event {
    kDeviceAdded = 1,
    kDeviceRemoved
};

struct Device {
    uint32_t handle;
    std::string udid;
};

struct DeviceNotification {
    Event event;
    std::shared_ptr<Device> device;
};

struct Connection {
    std::shared_ptr<Device> device;
    int fd;
    int error;
};

class Madmux {
public:
    using DevicesChangedCallback = std::function<void(const int)>;
    using DeviceConnectedCallback = std::function<void()>;
    using DeviceDisconnectedCallback = std::function<void(const int)>;

private:
    using DeviceNotificationCallback = std::function<void(const DeviceNotification&)>;

public:
	madmux_API Madmux();
	madmux_API ~Madmux();

	madmux_API void connect(const uint16_t port,
                            DevicesChangedCallback devices_changed,
                            DeviceConnectedCallback device_connected,
                            DeviceDisconnectedCallback device_disconnected);
	madmux_API bool connected() const;

	madmux_API uint32_t send(const uint8_t* data, const uint32_t length);
	madmux_API uint32_t receive(const uint8_t* data, const uint32_t length, const uint32_t timeout = 5000);

	madmux_API void setDebug(bool enabled);

private:
    void addDevice(const std::shared_ptr<Device> device);
    void removeDevice(const std::shared_ptr<Device> device);

    DevicesChangedCallback _devices_changed;
    DeviceConnectedCallback _device_connected;
    DeviceDisconnectedCallback _device_disconnected;

    DeviceNotificationCallback _device_notification;

    uint16_t _port = 0;

    bool _process_connect_thread = true;
    std::thread             _connect_thread;
    std::mutex              _connect_thread_mutex;
    std::condition_variable _connect_thread_cv;

    std::mutex _mutex;
    std::vector<std::shared_ptr<Device>> _devices;
    std::shared_ptr<Connection> _connection;
};

}
